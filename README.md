# Dubcat client manual installation

## Prerequisites

- Mincraft launcher that supports MC v1.14.4

## Installation

## Step 1

1. Navigate to your Minecraft installation folder (usually located at `%appdata%/.minecraft` on Windows)
2. Create a new folder named `1.14.4-DubcatClient` inside `versions` folder
3. Download `https://static.dubcat.cz/download/client/1.14/vanila-client.jar` and put it into `1.14.4-DubcatClient` folder that you've created in the previous step
4. Rename `vanila-client.jar` file to `1.14.4-DubcatClient.jar`
5. Download `https://static.dubcat.cz/download/client/1.14/DubcatClient.json` and put it into `1.14.4-DubcatClient` folder
6. Rename `DubcatClient.json` file to `1.14.4-DubcatClient.json`

At the end you should have 2 files inside `~mincraftfolder/versions/1.14.4-DubcatClient/`, the file tree should look like this:


```
mincraftfolder
│
└───versions
|    │
|    └───1.14.4-DubcatClient
|        |  1.14.4-DubcatClient.jar
|        |  1.14.4-DubcatClient.json
```

## Step 2

1. Navigate to `~mincraftfolder/libraries`
2. Create folder `cz` and open it
3. Create two folders `dubcat` and `launchwrapper-of` inside `cz` folder
4. Go inside `dubcat` folder and create `client` folder
5. Go inside `launchwrapper-of` folder and create `2.1` folder

You should have following nested folder structure

```
mincraftfolder
│
└───libraries
|    │
|    └───cz
|       │
|       └───dubcat
|       |   │
|       |   └───client
|       │
|       └───launchwrapper-of
|           │
|           └───2.1
```


6. Download `https://static.dubcat.cz/download/client/1.14/dubcat-client.jar` and put it into `cz/dubcat/client` folder
7. Download `https://static.dubcat.cz/download/client/1.14/launchwrapper-of-2.1.jar` and put it into `cz/launchwrapper-of/2.1` folder

### Step 3

You are almost done :) this is a final step, which requres you to edit `launcher_profiles.json` located at the root of your `minecraftfolder`

Add following profile to the `profiles` array inside `launcher_profiles.json` and save it

```json
"1.14.4-DubcatClient" : {
  "created" : "2019-10-04T16:29:51.529Z",
  "icon" : "Furnace",
  "lastUsed" : "2019-10-27T12:04:02.110Z",
  "lastVersionId" : "1.14.4-DubcatClient",
  "name" : "DubcatClient",
  "type" : "custom"
}
```
**!!BEWARE!!** If profile you added is NOT the last item in the array, dont forget to add comma to the array item. 

After editing `launcher_profiles.json` verify it through https://jsonlint.com/ (just copy/paste the contents there) that your JSON file is correct, if it says `Valid JSON` you are done!
If it says parse error, you've made a JSON syntax error somewhere.

Thats it, you should be able to launch Dubcat client.
